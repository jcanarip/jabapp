import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
      path: '',
      redirectTo: 'inicio',
      pathMatch: 'full'
    },
    {
      path: 'inicio',
      loadChildren: () => import('./inicio/inicio.module').then(m => m.InicioPageModule)
    },
    {
      path: 'paso2',
      loadChildren: () => import('./paso2/paso2.module').then(m => m.Paso2PageModule)
    },
    {
      path: 'paso3',
      loadChildren: () => import('./paso3/paso3.module').then(m => m.Paso3PageModule)
    },
    {
      path: 'paso4',
      loadChildren: () => import('./paso4/paso4.module').then(m => m.Paso4PageModule)
    },
    {
      path: 'paso5',
      loadChildren: () => import('./paso5/paso5.module').then(m => m.Paso5PageModule)
    },
    {
      path: 'welcome',
      loadChildren: () => import('./welcome/welcome.module').then(m => m.WelcomePageModule)
    },
    {
      path: 'event1',
      loadChildren: () => import('./event1/event1.module').then(m => m.Event1PageModule)
    },
    {
      path: 'eventcreator1',
      loadChildren: () => import('./eventcreator1/eventcreator1.module').then(m => m.Eventcreator1PageModule)
    },
    {
      path: 'profile',
      loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule)
    },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardPageModule' },
    { path: 'options', loadChildren: './options/options.module#OptionsPageModule' },
    { path: 'profile0', loadChildren: './profile0/profile0.module#Profile0PageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, Platform } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFireDatabase } from '@angular/fire/database';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

import { Event1Page } from '../event1/event1.page';
import { ProfilePage } from '../profile/profile.page';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

    slideOpts1 = {
        slidesPerView: 1.3,
        freeMode: true
    };
    etiquetas : any = [];
    cards : any = [];
    all_cards : any = [];
    tops : any = [];
    user : any = {};
    markers : any = {};
    users;

    constructor(
        private platform: Platform,
        private http: HTTP,
        private http_local: HttpClient,
        public modalCtrl: ModalController,
        private ionic_storage: Storage,
        public navCtrl: NavController,
        public afDB: AngularFireDatabase,
        private ga: GoogleAnalytics
    ) {
        this.ga.startTrackerWithId('UA-156255675')
        .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackView('dashboard');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));

        this.users = this.afDB.database.ref('/users');

        this.ionic_storage.get('user').then(
            (user) => {
                console.log('TRAYENDO USER', user)
                if (user == null) {
                    this.navCtrl.navigateRoot('/inicio');
                } else if (user.alias == undefined) {
                    this.navCtrl.navigateRoot('/profile0');
                } else {
                    this.user = user;
                    if (this.user.image == undefined || this.user.image == 'no-image' || this.user.image == '') {
                        this.user.image = 'assets/imgs/usuario.svg'
                    }
                    if (this.user.favorites == undefined) this.user.favorites = [];
                    console.log('userx', this.user)
                }
            }
        )

        this.refreshCards()

    }

    ngOnInit() {
    }

    refreshCards() {
        this.afDB.object('cards/').valueChanges().subscribe(
            (val) => {
                var maximo = 0
                var total = Object.keys(val).length
                var tops = []
                var markers = {}
                Object.keys(val).forEach(key=>{
                    maximo++
                    if (val[key]['tags']) {
                        for (let i = 0; i < val[key]['tags'].length; i++) {
                            if (this.user.favorites!=undefined && this.user.favorites.includes(key)) {
                                markers[key] = 'assets/imgs/favoritos/favoritos.svg';
                            } else {
                                markers[key] = 'assets/imgs/marker.svg';
                            }
                            if (this.cards[val[key]['tags'][i]] == undefined) {
                                this.cards[val[key]['tags'][i]] = [
                                    val[key]
                                ]
                            } else {
                                this.cards[val[key]['tags'][i]].push(val[key])
                            }
                            this.all_cards[key] = val[key]
                        }
                    }
                    this.markers = markers;
                    if (maximo > (total - 5)) {
                        tops.push(val[key])
                    }
                });
                this.tops = tops.reverse();
                console.log('this.tops', this.tops)

                if (this.platform.is('cordova')) {

                    this.http.get('https://jab.jorgecanari.com/wp-json/filtros/v1/etiquetas', {}, {})
                    .then(data => {

                        var object_data = JSON.parse(data.data)
                        console.log('----this.markers', this.markers)
                        this.etiquetas = object_data['etiquetas'];
                        console.log('----this.etiquetas', this.etiquetas)
                    })
                    .catch(error => {
                        console.log('error de filtros', JSON.stringify(error))
                    });

                } else {

                    this.http_local.get('https://jab.jorgecanari.com/wp-json/filtros/v1/etiquetas').subscribe(data => {
                        console.log('----this.markers', this.markers)
                        this.etiquetas = data['etiquetas']
                    }, err => {
                        console.log('error de filtros', JSON.stringify(err))
                    })

                }

            }
        )
    }

    async modal(page, data, callback) {
        const modal = await this.modalCtrl.create({
            component : page,
            componentProps : data
        });
        modal.onDidDismiss().then(callback);
        return await modal.present();
    }
    openEvent(card) {
        console.log('openEvent', this.all_cards)
        this.modal(Event1Page, {
            user : this.user,
            card : card,
            cards : this.all_cards
        }, (data) => {
            console.log('close!')
            this.refreshCards()
        })
    }
    goToProfile() {
        this.modal(ProfilePage, {
            user : this.user,
            cards : this.all_cards
        }, (data) => {
            console.log('close!')
        })
    }
    addFavorite(card) {
        if (this.user.favorites.includes(card.key)) {
            for( var i = 0; i < this.user.favorites.length; i++){
               if ( this.user.favorites[i] === card.key) {
                 this.user.favorites.splice(i, 1);
               }
            }
            this.markers[card.key] = 'assets/imgs/marker.svg';
        } else {
            this.user.favorites.push(card.key)
            this.markers[card.key] = 'assets/imgs/favoritos/favoritos.svg';
        }
        this.users.child(this.user.key).update(this.user);
        this.ionic_storage.set('user', this.user);
        console.log('favorites', this.user.favorites)
    }

}

import { Component, OnInit } from '@angular/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paso5',
  templateUrl: './paso5.page.html',
  styleUrls: ['./paso5.page.scss'],
})
export class Paso5Page implements OnInit {
  swipeOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };
  constructor(
      private ga: GoogleAnalytics,
      private router: Router
  ) {
      this.ga.startTrackerWithId('UA-156255675')
      .then(() => {
          console.log('Google analytics is ready now');
          this.ga.trackView('inicio 3');
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e));

      setTimeout(()=>{
          this.router.navigateByUrl('/login');
      }, 5000);
  }

  ngOnInit() {
  }

}

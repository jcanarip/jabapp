import { Component } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    public appPages = [
        {
            title: 'Home',
            url: '/home',
            icon: 'home'
        },
        {
            title: 'List',
            url: '/list',
            icon: 'list'
        }
    ];

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public navCtrl: NavController,
        private ionic_storage: Storage,
        private ga: GoogleAnalytics
    ) {
        this.ga.startTrackerWithId('UA-156255675')
        .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackView('app');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));

        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // this.statusBar.styleDefault();
            this.statusBar.backgroundColorByName('white');
            this.ionic_storage.get('user').then(
                (user) => {
                    this.splashScreen.hide();
                    console.log('inicio', user);
                    if (user != null) {
                        this.navCtrl.navigateRoot('/welcome');
                    } else {
                        this.navCtrl.navigateRoot('/paso3');
                    }
                }
            );
        });
    }
}

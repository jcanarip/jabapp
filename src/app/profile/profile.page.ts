import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, NavParams } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { OptionsPage } from '../options/options.page';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  swipeOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };
  slideOpts2 = {
    slidesPerView: 1.6,
    freeMode: true
  };
  user : any = {};
  cards : any = {};
  favorites : any = [];
  grafos : any = [];
  points : number = 0;

  constructor(
      public navCtrl: NavController,
      public modalCtrl: ModalController,
      public navParams: NavParams,
      private ionic_storage: Storage
  ) {

      this.ionic_storage.get('user').then(
          (user) => {
              if (user == null) {
                  this.navCtrl.navigateRoot('/inicio');
              } else {
                  this.user = user;
                  console.log('profile', this.user)
                  if (this.user.favorites == undefined) this.user.favorites = [];


                  this.cards = navParams.get('cards');
                  if (this.user.image == undefined || this.user.image == 'no-image' || this.user.image == '') {
                      this.user.image = 'assets/imgs/usuario.svg'
                  }
                  Object.keys(this.cards).forEach(key=>{
                      if (this.user.favorites.includes(key)) {
                          this.favorites.push(this.cards[key])
                      }
                  });
                  console.log('this.favorites', this.favorites)
                  if (this.user.jabs != undefined) {
                      var total = Object.keys(this.user.jabs).length
                      var maximo = 0
                      var c = 0;
                      Object.keys(this.user.jabs).forEach(key=>{
                          maximo++
                          console.log(key, this.user.jabs[key])
                          this.points += this.user.jabs[key]
                          if (maximo > (total - 7)) {
                              c++;
                              let f = key.split('-')
                              this.grafos.push({
                                  id : c,
                                  date : f[2]+'/'+f[1],
                                  points : this.user.jabs[key]
                              })
                          }
                      });
                  } else {
                      this.points = 0;
                  }


              }
          }
      )

  }

  ngOnInit() {
  }

  async modal2(page, data, callback) {
      const modal = await this.modalCtrl.create({
          component : page,
          componentProps : data
      });
      modal.onDidDismiss().then(callback);
      return await modal.present();
  }

  onEditar(){
      this.modal2(OptionsPage, {
          user : this.user
      }, (data) => {
          if (data.data.action != undefined && data.data.action == 'logout') {
              setTimeout(()=>{
                  this.modalCtrl.dismiss();
              }, 100);
          }
      })
  }
  onCerrar(){
     this.modalCtrl.dismiss();
  }

  cambiarFoto(){
    console.log("Cambiar foto");
  }

}

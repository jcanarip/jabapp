import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@Component({
  selector: 'app-paso3',
  templateUrl: './paso3.page.html',
  styleUrls: ['./paso3.page.scss'],
})
export class Paso3Page implements OnInit {
  swipeOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };
  texto : string = '';

  constructor(
      private platform: Platform,
      private http: HTTP,
      private http_local: HttpClient,
      private router: Router,
      private ga: GoogleAnalytics
  ) {
      this.ga.startTrackerWithId('UA-156255675')
      .then(() => {
          console.log('Google analytics is ready now');
          this.ga.trackView('inicio 1');
      })
      .catch(e => console.log('Error starting GoogleAnalytics', e));

      if (this.platform.is('cordova')) {

          this.http.get('http://jab.jorgecanari.com/wp-json/opciones/v1/texto-inicial', {}, {})
          .then(data => {

              var object_data = JSON.parse(data.data)
              console.log('object_data', object_data)
              this.texto = object_data['texto'];
              this.changeView(object_data['tiempo'])
          })
          .catch(error => {
              console.log('error de filtros', JSON.stringify(error))
          });

      } else {

          this.http_local.get('http://jab.jorgecanari.com/wp-json/opciones/v1/texto-inicial').subscribe(data => {
              console.log('data', data)
              this.texto = data['texto']
              this.changeView(data['tiempo'])
          }, err => {
              console.log('error de filtros', JSON.stringify(err))
          })

      }

  }

  changeView(time) {
      setTimeout(()=>{
          this.router.navigateByUrl('/inicio');
      }, (time * 1000));
  }

  ngOnInit() {
  }

}

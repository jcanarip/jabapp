import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@Component({
  selector: 'app-profile0',
  templateUrl: './profile0.page.html',
  styleUrls: ['./profile0.page.scss'],
})
export class Profile0Page implements OnInit {
  swipeOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };
  slideOpts2 = {
    slidesPerView: 3.6,
    freeMode: true
  };
  slideOpts3 = {
    slidesPerView: 1.3,
    freeMode: true
  };
  alias : string = ''
  posicion : string = ''
  user : any = {}
  clase : any = {}
  ref;

  constructor(
      public navCtrl: NavController,
      public alertCtrl: AlertController,
      private ionic_storage: Storage,
      public afDB: AngularFireDatabase,
      private camera : Camera,
      private ga: GoogleAnalytics
    ) {
        this.ga.startTrackerWithId('UA-156255675')
        .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackView('profile0');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));

        this.ionic_storage.get('user').then(
            (user) => {
                if (user == null) {
                    this.navCtrl.navigateRoot('/inicio');
                } else {
                    this.user = user;
                    this.user.alias = '';
                    this.user.posicion = '';
                    console.log('profile0', this.user)
                }
            }
        )
        this.clase = {
            liberal : '',
            derecha : '',
            izquierda : '',
            nose : ''
        }
        this.ref = this.afDB.database.ref('/users');

    }

  ngOnInit() {
  }
  onPosicion(key, posicion){
      this.clase = {
          liberal : '',
          derecha : '',
          izquierda : '',
          nose : ''
      }
      this.clase[key] = 'active'
      this.user.posicion = posicion
  }
  async presentAlert(title, message, buttons) {
      const alert = await this.alertCtrl.create({
          header: title,
          message: message,
          buttons: buttons
      });

      await alert.present();
  }
  personalizar() {
      if (this.user.alias == '') {
          this.presentAlert('¡Detente!', 'Debes completar tu alias para continuar', [
            {
              text: 'Cerrar',
              handler: () => {}
            }
          ])
      } else if (this.user.posicion == '') {
          this.presentAlert('¡Detente!', 'Debes elegir tu posición para continuar', [
            {
              text: 'Cerrar',
              handler: () => {}
            }
          ])
      } else {
          this.ref.child(this.user.key).update(this.user);
          this.ionic_storage.set('user', this.user);
          console.log('guardado', this.user)
          this.navCtrl.navigateRoot('/dashboard');
      }
  }
  openPicture($event) {
      console.log('event', $event.target.value);
      if ($event.target.value == 'take') {
          this.takePicture();
      } else {
          this.selectPicture();
      }
  }
  takePicture() {
      const options : CameraOptions = {
          quality: 50,
          targetHeight: 600,
          targetWidth: 360,
          destinationType: this.camera.DestinationType.DATA_URL,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((imageData) => {
          if (this.user.image == undefined) ''
          this.user.image = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
          console.log('error', err);
      });
  }
  selectPicture() {
      const options : CameraOptions = {
          quality: 50,
          targetHeight: 600,
          targetWidth: 360,
          sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
          destinationType: this.camera.DestinationType.DATA_URL,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((imageData) => {
          if (this.user.image == undefined) ''
          this.user.image = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
          console.log('error', err);
      });
  }

}

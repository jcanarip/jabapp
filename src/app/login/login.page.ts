import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';
// import { Facebook } from '@ionic-native/facebook/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Platform, NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';

import { AuthService } from '../../services/auth.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
    providers: [AuthService]
})
export class LoginPage implements OnInit {
    user:any = {};
    userdata: string;

    usergoogle:any = {};
    password = '@Seguridad#2019@'
    ref;

    constructor(
        private platform: Platform,
        public navCtrl: NavController,
        private fb: Facebook,
        private http: HttpClient,
        private googlePlus: GooglePlus,
        private auth: AuthService,
        private router: Router,
        private storage: Storage,
        public afDB: AngularFireDatabase,
        private ga: GoogleAnalytics
    ) {
        this.ga.startTrackerWithId('UA-156255675')
        .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackView('login');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));

        this.ref = this.afDB.database.ref('/users');
    }

    ngOnInit() {
    }

    loginFb(){
        if (this.platform.is('cordova')) {

            // Login en celular

            this.fb.login(['public_profile', 'email'])
            .then((res: FacebookLoginResponse) => {
                console.log('Logged into Facebook!', res)
                if(res.status==='connected'){
                    this.user.img = 'https://graph.facebook.com/'+res.authResponse.userID+'/picture?type=square';
                    this.getData(res.authResponse.accessToken);
                } else {
                    alert('Login Failed')
                }
            })
            .catch(e => console.log('Error logging into Facebook', e));

        } else {
            var credentials = {
                name : 'Jhon Doe5',
                email : 'prueba5@jab.com',
                password : this.password,
                image : 'assets/imgs/usuario.svg'
            };
            console.log('CREDENCIALES LOCAL', credentials)
            this.login(credentials)
        }
    }

    login(credentials) {
        this.auth.signUp(credentials).then(
            (response) => {
                console.log('responsa', response)
                this.auth.saveInitialData(credentials.email, response.user.uid, credentials.email, 0, credentials.image).then(
                    (response) => {
                        credentials.key = response.path.pieces_[1]
                        this.ref.child(credentials.key).update({
                            key : credentials.key
                        });
                        this.storage.set('user', credentials);
                        this.router.navigateByUrl('/dashboard');
                    },
                    error => {
                        alert(error.message)
                    }
                );
            },
            error => {
                if (error.code == "auth/email-already-in-use") {
                    this.auth.validateUserCode(credentials, (snapshot) => {
                        let userObject = snapshot.val();
                        var id_user = Object.keys(userObject)[0];
                        let user = userObject[id_user]
                        console.log('el usuario ya existe', user)
                        this.storage.set('user', user);
                        this.router.navigateByUrl('/dashboard');
                    })
                } else {
                    alert(error.message)
                }
            }
        );
    }

    getData(access_token:string){
        let url = 'https://graph.facebook.com/me?=fields=id,name,first_name,last_name,email&access_token='+access_token;
        this.http.get(url).subscribe(data =>{
            console.log('DATOS DE FACEBOOKKK', JSON.stringify(data));
            // {"name":"Cesar J. Aquino Maximiliano","id":"10220377540899148"};
            var credentials = {
                name : data['name'],
                email : data['id']+'@facebook.com',
                password : this.password,
                image : this.user.img
            };
            console.log('CREDENCIALES FACEBOOK', JSON.stringify(credentials))
            this.login(credentials)
        })
    }

    loginGoogle(){
        if (this.platform.is('cordova')) {
            this.googlePlus.login({})
            .then(res => {
                console.log('DATOS DE GOOGLE!!!', JSON.stringify(res));
                this.usergoogle = res;
                var credentials = {
                    name : res.displayName,
                    email : res.userId+'@google.com',
                    password : this.password,
                    image : res.imageUrl
                };
                console.log('CREDENCIALES GOOGLE', credentials)
                this.login(credentials)
            })
            .catch(err => console.error(err));
        } else {
            var credentials = {
                name : 'Jhon Doe',
                email : 'prueba@jab.com',
                password : this.password,
                image : 'assets/imgs/usuario.svg'
            };
            console.log('CREDENCIALES LOCAL', credentials)
            this.login(credentials)
        }
    }

}

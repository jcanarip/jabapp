import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';


import { IonBottomDrawerModule } from '../modules/ion-bottom-drawer/ion-bottom-drawer.module';


const routes: Routes = [
  {
    path: '',
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonBottomDrawerModule,
    RouterModule.forChild(routes)
  ],
  declarations: []
})
export class Event1PageModule {}

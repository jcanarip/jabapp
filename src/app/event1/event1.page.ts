import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, AlertController } from '@ionic/angular';
import * as $ from 'jquery';
import { DrawerState } from '../modules/ion-bottom-drawer/drawer-state';
import { AngularFireDatabase } from '@angular/fire/database';
import { Storage } from '@ionic/storage';
import { formatDate } from '@angular/common';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

import { Eventcreator1Page } from '../eventcreator1/eventcreator1.page';

@Component({
    selector: 'app-event1',
    templateUrl: './event1.page.html',
    styleUrls: ['./event1.page.scss'],
})
export class Event1Page implements OnInit {
    swipeOpts = {
        allowSlidePrev: false,
        allowSlideNext: false
    };

    slideOpts2 = {
        slidesPerView: 1.6,
        freeMode: true
    };

    shouldBounce = true;
    disableDrag = false;
    dockedHeight = 350;
    distanceTop = 56;
    drawerState = DrawerState.Bottom;
    states = DrawerState;
    minimumHeight = 0;
    card : any = {};
    user : any = {};
    respuestas : any = [];
    users;
    db_cards;
    all_cards;
    today : string = '';
    cont:number = 0;

    constructor(
        public modalCtrl: ModalController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        private storage: Storage,
        public afDB: AngularFireDatabase,
        private ga: GoogleAnalytics
    ) {
        this.ga.startTrackerWithId('UA-156255675')
        .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackView('event_detail');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));

        this.user = navParams.get('user');
        this.card = navParams.get('card');
        this.all_cards = navParams.get('cards');
        if (this.card.jabs == undefined) this.card.jabs = 0;
        if (this.card.childs == undefined) this.card.childs = [];
        this.users = afDB.database.ref('/users');
        this.db_cards = afDB.database.ref('/cards');
        console.log('elegido', this.card)

        this.today = formatDate(new Date(), 'yyyy-MM-dd', 'en');

        Object.keys(this.all_cards).forEach(key=>{
            if (this.card.childs.includes(key)) {
                if (this.all_cards[key]['jabs'] == undefined) this.all_cards[key]['jabs'] = 0
                this.respuestas.push(this.all_cards[key])
            }
        });
    }

    ngOnInit() {
    }
    async modal(page, data, callback) {
        const modal = await this.modalCtrl.create({
            component : page,
            componentProps : data
        });
        modal.onDidDismiss().then(callback);
        return await modal.present();
    }
    openEvent(card) {
        this.modal(Event1Page, {
            user : this.user,
            card : card,
            cards : this.all_cards
        }, (data) => {
            console.log('close!')
        })
    }
    lanzarJab(){

        $('.progress-value').css('transform', 'scale(1.1)')
        this.cont += 1;
        //console.log(this.cont);
        if(this.cont==1){
          $('.progress .progress-right .progress-bar').css('animation-name', 'loading-1');
        } else if(this.cont==2){
          $('.progress .progress-right .progress-bar').css('animation-name', 'loading-2');
        } else if(this.cont==3){
          $('.progress .progress-right .progress-bar').css('animation-name', 'loading-3');
        } else if(this.cont==4){
          $('.progress .progress-right .progress-bar').css('animation-name', 'loading-4');
        } else if(this.cont==5){
          $('.progress .progress-right .progress-bar').css('animation-name', 'loading-5');
        } else if(this.cont==6){
          $('.progress .progress-right .progress-bar').css('animation-name', 'loading-6');
        } else if(this.cont==7){
          $('.progress .progress-right .progress-bar').css('animation-name', 'loading-7');
        } else if(this.cont==8){
          $('.progress .progress-right .progress-bar').css('animation-name', 'loading-8');
        } else if(this.cont==9){
          $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-1');
        } else if(this.cont==10){
          $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-2');
        } else if(this.cont==11){
          $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-3');
        } else if(this.cont==12){
          $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-4');
        } else if(this.cont==13){
          $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-5');
        } else if(this.cont==14){
          $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-6');
        } else if(this.cont==15){
          $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-7');
        } else if(this.cont==16){
          $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-8');
        } else {

        }


        if (this.user.jabs == undefined) this.user['jabs'] = {}
        if (this.user.jabs[this.today] == undefined) this.user.jabs[this.today] = 0;
        if (this.user.jabs[this.today] < 1000) {
            this.card.jabs += 1
            this.user.jabs[this.today] += 1
            setTimeout(function(){
                $('.progress-value').css('transform', 'scale(1)')
            }, 100);

            // Guardar los jabs
            let jabs = this.card.jabs;
            setTimeout(() => {
                if (jabs == this.card.jabs) {
                    if (this.card.jabs) {
                        console.log('guardar jab', this.card)
                        this.db_cards.child(this.card.key).update({
                            jabs : this.card.jabs
                        });
                        console.log('card guardado', this.card)
                        this.users.child(this.user.key).update({
                            jabs : this.user.jabs
                        });
                        this.storage.set('user', this.user);
                    }
                }
            }, 2000);
        } else {
            this.presentAlert('¡Perdón!', 'Has llegado al límite de jabs por hoy', [
              {
                text: 'Cerrar',
                handler: () => {
                    this.modalCtrl.dismiss();
                }
              }
            ])
        }

    }
    async presentAlert(title, message, buttons) {
        const alert = await this.alertCtrl.create({
            header: title,
            message: message,
            buttons: buttons
        });

        await alert.present();
    }
    listaRespuestas(){
        console.log("respuestas", this.respuestas);
        //this.drawerState===this.states.Docked;
        this.shouldBounce = true;
        this.disableDrag = false;
        this.dockedHeight = 400;
        this.distanceTop = 56;
        this.drawerState = DrawerState.Docked;
        this.states = DrawerState;
        this.minimumHeight = 0;
    }

    compartir(){
        console.log("compartir");
    }

    onCerrar(){
        this.modalCtrl.dismiss();
    }

    async openmodal(page, data, callback) {
        const modal = await this.modalCtrl.create({
            component : page,
            componentProps : data
        });
        modal.onDidDismiss().then(callback);
        return await modal.present();
    }

    crearRespuesta(){
        this.openmodal(Eventcreator1Page, {
            user : this.user,
            parent : this.card
        }, (data) => {
            console.log('close!')
        })
    }

}

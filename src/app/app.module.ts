import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { IonicStorageModule } from '@ionic/storage'
import { Camera } from '@ionic-native/camera/ngx';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireAuth } from 'angularfire2/auth';
//
import { firebaseConfig } from '../config';

import { IonBottomDrawerModule } from './modules/ion-bottom-drawer/ion-bottom-drawer.module';
import { Event1Page } from './event1/event1.page';
import { Eventcreator1Page } from './eventcreator1/eventcreator1.page';
// import { SharedModule } from './modules/sharedModule';
import { ProfilePageModule } from './profile/profile.module';
import { OptionsPageModule } from './options/options.module';

import { Facebook } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@NgModule({
  declarations: [
      AppComponent,
      Event1Page,
      Eventcreator1Page,
  ],
  entryComponents: [
      Event1Page,
      Eventcreator1Page,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonBottomDrawerModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    // SharedModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseConfig.fire),
    ProfilePageModule,
    OptionsPageModule
  ],
  providers: [
    StatusBar,
    HTTP,
    SplashScreen,
    AngularFireDatabase,
    AngularFireAuthModule,
    AngularFireAuth,
    Facebook,
    GooglePlus,
    GoogleAnalytics,
    Camera,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

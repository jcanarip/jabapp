import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { formatDate } from '@angular/common';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.page.html',
    styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
    swipeOpts = {
        allowSlidePrev: false,
        allowSlideNext: false
    };
    today : string = '';
    user : any = {};
    jabs : number = 0

    constructor(
        private ionic_storage: Storage,
        public navCtrl: NavController,
        private router: Router,
        private ga: GoogleAnalytics
    ) {
        this.ga.startTrackerWithId('UA-156255675')
        .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackView('welcome');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));

        this.today = formatDate(new Date(), 'yyyy-MM-dd', 'en');

        this.ionic_storage.get('user').then(
            (user) => {
                console.log('TRAYENDO USER', user)
                if (user == null) {
                    this.navCtrl.navigateRoot('/inicio');
                } else if (user.alias == undefined) {
                    this.navCtrl.navigateRoot('/profile0');
                } else {
                    this.user = user;
                    if (this.user.jabs[this.today] == undefined) this.user.jabs[this.today] = 0;
                    this.jabs = this.user.jabs[this.today]
                }
            }
        )

        setTimeout(()=>{
            this.router.navigateByUrl('/dashboard');
        }, 5000);
    }

    ngOnInit() {
    }

}

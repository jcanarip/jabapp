import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, NavParams } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@Component({
    selector: 'app-options',
    templateUrl: './options.page.html',
    styleUrls: ['./options.page.scss'],
})
export class OptionsPage implements OnInit {

    user : any = {};
    ref;

    constructor(
        public navCtrl: NavController,
        private ionic_storage: Storage,
        public modalCtrl: ModalController,
        public navParams: NavParams,
        public afDB: AngularFireDatabase,
        private ga: GoogleAnalytics
    ) {
        this.ga.startTrackerWithId('UA-156255675')
        .then(() => {
            console.log('Google analytics is ready now');
            this.ga.trackView('options');
        })
        .catch(e => console.log('Error starting GoogleAnalytics', e));

        this.user = navParams.get('user');
        if (this.user.image == undefined || this.user.image == 'no-image' || this.user.image == '') {
            this.user.image = 'assets/imgs/usuario.svg'
        }
        console.log('user traido', this.user)
        this.ref = this.afDB.database.ref('/users');
    }

    ngOnInit() {
    }
    typing() {
        let wordSearch = this.user.alias;
        setTimeout(() => {
            if (wordSearch == this.user.alias) {
                if (this.user.alias) {
                    this.save()
                }
            }
        }, 2000);
    }
    save() {
        this.ref.child(this.user.key).update(this.user);
        this.ionic_storage.set('user', this.user);
        console.log('guardado', this.user)
    }
    logout() {
        this.ionic_storage.set('user', null);
        this.modalCtrl.dismiss({
            action : 'logout'
        });
        this.navCtrl.navigateRoot('/inicio');
    }
    onCerrar(){
       this.modalCtrl.dismiss();
    }

}

import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';

@Injectable()
export class AuthService {
	itemRef: AngularFireObject<any>;
	name;  displayDetail;  updatedname; ref;
	info_user : any = {};
	sync : number = 0;

	constructor(public afAuth: AngularFireAuth, public afDB: AngularFireDatabase) {
		this.itemRef = afDB.object('users');
		this.ref = this.afDB.database.ref('/users');
		// this.getDetails();
	}

	signInWithEmail(credentials) {
		console.log('Sign in with email');
		return this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password);
	}

	signUp(credentials) {
		return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password);
	}

	saveInitialData(name, uid, email, onesignal_player_id, image){
		let info = {
			name : name,
			uid : uid,
			email : email,
			image : image,
			points : 0,
			badges : 0,
			winned : 0,
			counters : 0,
			jabs : {},
			favorites : [],
			onesignal_player_id : onesignal_player_id
		};
		return this.ref.push(info);
	}

	regenerateUserCode(email, newcode) {
		var afDB = this.afDB;
		this.ref.orderByChild("email").equalTo(email).on('value', (snapshot) => {
			if (this.sync == 0) {
				let key = Object.keys(snapshot.val());
				console.log('keyx: '+key[0], this.sync);
				afDB.list('/users').update(key[0],{ code: newcode })
				this.sync = 1;
			}
		})
	}

	validateUserCode(credentials, callback) {
		this.ref.orderByChild("email").equalTo(credentials.email).on('value', callback)
	}

}

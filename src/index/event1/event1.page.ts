import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, AlertController } from '@ionic/angular';
import * as $ from 'jquery';
import { DrawerState } from '../modules/ion-bottom-drawer/drawer-state';
import { AngularFireDatabase } from '@angular/fire/database';
import { Storage } from '@ionic/storage';
import { formatDate } from '@angular/common';

import { Eventcreator1Page } from '../eventcreator1/eventcreator1.page';

@Component({
    selector: 'app-event1',
    templateUrl: './event1.page.html',
    styleUrls: ['./event1.page.scss'],
})
export class Event1Page implements OnInit {
    swipeOpts = {
        allowSlidePrev: false,
        allowSlideNext: false
    };

    slideOpts2 = {
        slidesPerView: 1.6,
        freeMode: true
    };

    shouldBounce = true;
    disableDrag = false;
    dockedHeight = 350;
    distanceTop = 56;
    drawerState = DrawerState.Bottom;
    states = DrawerState;
    minimumHeight = 0;
    card : any = {};
    user : any = {};
    users;
    cards;
    today : string = '';

    constructor(
        public modalCtrl: ModalController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        private storage: Storage,
        public afDB: AngularFireDatabase
    ) {
        this.user = navParams.get('user');
        this.card = navParams.get('card');
        if (this.card.jabs == undefined) this.card.jabs = 0;
        this.users = afDB.database.ref('/users');
        this.cards = afDB.database.ref('/cards');
        console.log('elegido', this.card)

        this.today = formatDate(new Date(), 'yyyy-MM-dd', 'en');
    }

    ngOnInit() {
    }

    lanzarJab(){

        $('.progress-value').css('transform', 'scale(1.1)')
        if (this.user.jabs == undefined) this.user['jabs'] = {}
        if (this.user.jabs[this.today] == undefined) this.user.jabs[this.today] = 0;
        if (this.user.jabs[this.today] < 1000) {
            this.card.jabs += 1
            this.user.jabs[this.today] += 1
            setTimeout(function(){
                $('.progress-value').css('transform', 'scale(1)')
            }, 100);

            // Guardar los jabs
            let jabs = this.card.jabs;
            setTimeout(() => {
                if (jabs == this.card.jabs) {
                    if (this.card.jabs) {
                        this.cards.child(this.card.key).update({
                            jabs : this.card.jabs
                        });
                        console.log('card guardado', this.card)
                        this.users.child(this.user.key).update({
                            jabs : this.user.jabs
                        });
                        this.storage.set('user', this.user);
                    }
                }
            }, 2000);
        } else {
            this.presentAlert('¡Perdón!', 'Has llegado al límite de jabs por hoy', [
              {
                text: 'Cerrar',
                handler: () => {
                    this.modalCtrl.dismiss();
                }
              }
            ])
        }

    }
    async presentAlert(title, message, buttons) {
        const alert = await this.alertCtrl.create({
            header: title,
            message: message,
            buttons: buttons
        });

        await alert.present();
    }
    listaRespuestas(){
        console.log("respuestas");
        //this.drawerState===this.states.Docked;
        this.shouldBounce = true;
        this.disableDrag = false;
        this.dockedHeight = 400;
        this.distanceTop = 56;
        this.drawerState = DrawerState.Docked;
        this.states = DrawerState;
        this.minimumHeight = 0;
    }

    compartir(){
        console.log("compartir");
    }

    responder(){
        console.log("responder");
    }

    onCerrar(){
        this.modalCtrl.dismiss();
    }

    async openmodal(page, data, callback) {
        const modal = await this.modalCtrl.create({
            component : page,
            componentProps : data
        });
        modal.onDidDismiss().then(callback);
        return await modal.present();
    }

    crearRespuesta(){
        this.openmodal(Eventcreator1Page, {
            user : this.user
        }, (data) => {
            console.log('close!')
        })
    }

}

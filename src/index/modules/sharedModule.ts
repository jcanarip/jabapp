import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonBottomDrawerModule } from './ion-bottom-drawer/ion-bottom-drawer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [
    IonBottomDrawerModule,
  ],
  providers: [],
  exports: [
    IonBottomDrawerModule
  ]
})

export class SharedModule {}

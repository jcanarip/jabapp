import { Component, OnInit } from '@angular/core';
import { Platform, ModalController, AlertController, NavParams } from '@ionic/angular';
import * as $ from 'jquery';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFireDatabase } from '@angular/fire/database';
import { DrawerState } from '../modules/ion-bottom-drawer/drawer-state';

@Component({
  selector: 'app-eventcreator1',
  templateUrl: './eventcreator1.page.html',
  styleUrls: ['./eventcreator1.page.scss'],
})
export class Eventcreator1Page implements OnInit {

  swipeOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };

  slideOpts2 = {
    slidesPerView: 3.6,
    freeMode: true
  };

  shouldBounce = true;
  disableDrag = false;
  dockedHeight = 250;
  distanceTop = 56;
  drawerState = DrawerState.Bottom;
  states = DrawerState;
  minimumHeight = 0;
  filtros : any = [];
  current_image : any = false;
  last_image : any = false;
  current_icon : string = '';
  current_tags : string = '';
  current_array_tags : any = [];
  text : string = '';
  tipo : string = 'imagen';
  cards;
  user : any = {};

  constructor(
      private platform: Platform,
      private http: HTTP,
      private http_local: HttpClient,
      public afDB: AngularFireDatabase,
      public modalCtrl: ModalController,
      public alertCtrl: AlertController,
      public navParams: NavParams
  ) {
      this.user = navParams.get('user');

      this.cards = afDB.database.ref('/cards');
      console.log('empezando a traer filtros 5')

      if (this.platform.is('cordova')) {

          this.http.get('https://jab.jorgecanari.com/wp-json/filtros/v1/all', {}, {})
          .then(data => {

            console.log('-----filtros traidos', data.data)
            var object_data = JSON.parse(data.data)
            this.filtros = object_data.filtros;
            this.current_image = object_data.filtros[0]['image'];
            this.current_icon = object_data.filtros[0]['icon'];
            var tags = []
            if (object_data.filtros[0]['tags'].length > 0) {
                for (let i = 0; i < object_data.filtros[0]['tags'].length; i++) {
                    tags.push('#'+object_data.filtros[0]['tags'][i])
                }
            }
            this.current_array_tags = tags;
            this.current_tags = tags.join(' - ');
            console.log('------filtros', this.filtros)

          })
          .catch(error => {
              console.log('error de filtros', JSON.stringify(error))
          });

      } else {

          this.http_local.get('https://jab.jorgecanari.com/wp-json/filtros/v1/all').subscribe(res => {
              console.log('filtros traidos', res)
              this.filtros = res['filtros'];
              this.current_image = res['filtros'][0]['image'];
              this.current_icon = res['filtros'][0]['icon'];
              var tags = []
              if (res['filtros'][0]['tags'].length > 0) {
                  for (let i = 0; i < res['filtros'][0]['tags'].length; i++) {
                      tags.push('#'+res['filtros'][0]['tags'][i])
                  }
              }
              this.current_array_tags = tags;
              this.current_tags = tags.join(' - ');
              console.log('filtros', this.filtros)
          }, err => {
              console.log('error de filtros', JSON.stringify(err))
          });

      }

  }

  ngOnInit() {

  }

  onTexto(){
    this.tipo = 'texto'
    this.last_image = this.current_image;
    this.current_image = false;
  }
  selectFiltro(filtro) {
      console.log('elegido', filtro)
      if (this.tipo == 'imagen') {
        this.current_image = filtro['image'];
        this.last_image = filtro['image'];
    } else {
        this.last_image = filtro['image'];
    }
      this.current_icon = filtro['icon'];
      var tags = []
      if (filtro['tags'].length > 0) {
          for (let i = 0; i < filtro['tags'].length; i++) {
              tags.push('#'+filtro['tags'][i])
          }
      }
      this.current_array_tags = tags;
      this.current_tags = tags.join(' - ');
  }

  onImagen(){
    this.current_image = this.last_image;
    this.tipo = 'imagen'
  }
  async presentAlert(title, message, buttons) {
      const alert = await this.alertCtrl.create({
          header: title,
          message: message,
          buttons: buttons
      });

      await alert.present();
  }
  onCerrar(){
    this.modalCtrl.dismiss();
  }

  onFiltro(){
    console.log("filtro")
    this.shouldBounce = true;
    this.disableDrag = false;
    this.dockedHeight = 250;
    this.distanceTop = 56;
    this.drawerState = DrawerState.Docked;
    this.states = DrawerState;
    this.minimumHeight = 0;
  }
  onCrear(){
    console.log("Crear: " + this.text, this.filtros);
    var card_key = this.cards.push({
        tipo : this.tipo,
        text : this.text,
        icon : this.current_icon,
        image : this.current_image,
        tags : this.current_array_tags,
        jabs : 0,
        user : {
            key : this.user.key,
            alias : this.user.alias,
            image : this.user.image
        }
    }).key;
    console.log('key creado', card_key)
    this.cards.child(card_key).update({
        key : card_key
    });

    this.presentAlert('¡Listo!', 'El card fue creado correctamente', [
      {
        text: 'Cerrar',
        handler: () => {
            this.modalCtrl.dismiss();
        }
      }
    ])
  }

  onaplicarFiltro(){
    console.log("Aplicar Filtro");
    $('.tag').show();
    $('.insignia_crear').show();
    $('.fondo_seleccionado').show();
  }

}

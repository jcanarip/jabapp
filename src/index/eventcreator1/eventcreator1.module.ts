import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { Eventcreator1Page } from './eventcreator1.page';

import { IonBottomDrawerModule } from '../modules/ion-bottom-drawer/ion-bottom-drawer.module';


const routes: Routes = [
  {
    path: '',
    component: Eventcreator1Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonBottomDrawerModule,
    RouterModule.forChild(routes)
  ],
  declarations: [Eventcreator1Page]
})
export class Eventcreator1PageModule {}

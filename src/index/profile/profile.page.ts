import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, NavParams } from '@ionic/angular';
import { Storage } from '@ionic/storage';

import { OptionsPage } from '../options/options.page';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  swipeOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };
  slideOpts2 = {
    slidesPerView: 1.6,
    freeMode: true
  };
  user : any = {};

  constructor(
      public navCtrl: NavController,
      public modalCtrl: ModalController,
      public navParams: NavParams,
      private ionic_storage: Storage
  ) {

      this.user = navParams.get('user');
      if (this.user.image == undefined || this.user.image == 'no-image') {
          this.user.image = 'assets/imgs/usuario.svg'
      }
  }

  ngOnInit() {
  }

  async modal2(page, data, callback) {
      const modal = await this.modalCtrl.create({
          component : page,
          componentProps : data
      });
      modal.onDidDismiss().then(callback);
      return await modal.present();
  }

  onEditar(){
      this.modal2(OptionsPage, {
          user : this.user
      }, (data) => {
          console.log('close!')
      })
  }
  onCerrar(){
     this.modalCtrl.dismiss();
  }

  cambiarFoto(){
    console.log("Cambiar foto");
  }

}

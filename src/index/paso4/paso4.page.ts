import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paso4',
  templateUrl: './paso4.page.html',
  styleUrls: ['./paso4.page.scss'],
})
export class Paso4Page implements OnInit {
  swipeOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };
  cont:number = 0;
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  lanzarJab(){

      $('.progress-value').css('transform', 'scale(.9)')
    this.cont += 1;
    //console.log(this.cont);
    if(this.cont==1){
      $('.progress .progress-right .progress-bar').css('animation-name', 'loading-1');
    } else if(this.cont==2){
      $('.progress .progress-right .progress-bar').css('animation-name', 'loading-2');
    } else if(this.cont==3){
      $('.progress .progress-right .progress-bar').css('animation-name', 'loading-3');
    } else if(this.cont==4){
      $('.progress .progress-right .progress-bar').css('animation-name', 'loading-4');
    } else if(this.cont==5){
      $('.progress .progress-right .progress-bar').css('animation-name', 'loading-5');
    } else if(this.cont==6){
      $('.progress .progress-right .progress-bar').css('animation-name', 'loading-6');
    } else if(this.cont==7){
      $('.progress .progress-right .progress-bar').css('animation-name', 'loading-7');
    } else if(this.cont==8){
      $('.progress .progress-right .progress-bar').css('animation-name', 'loading-8');
    } else if(this.cont==9){
      $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-1');
    } else if(this.cont==10){
      $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-2');
    } else if(this.cont==11){
      $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-3');
    } else if(this.cont==12){
      $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-4');
    } else if(this.cont==13){
      $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-5');
    } else if(this.cont==14){
      $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-6');
    } else if(this.cont==15){
      $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-7');
    } else if(this.cont==16){
      $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-8');
      this.router.navigateByUrl(`/paso5`);
    } else {

    }
    setTimeout(function(){
        $('.progress-value').css('transform', 'scale(1)')
    }, 100);

  }

}

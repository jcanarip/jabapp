import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  swipeOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };
  cont:number = 0;
  estado : string = 'pausa'
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }
  lanzarJab(){
    $('.progress-value').css('transform', 'scale(1.1)')
    this.estado = 'golpe'
    this.cont += 1;
    //console.log(this.cont);
    if(this.cont==1){
      $('.progress .progress-right .progress-bar').css('animation-name', 'loading-1');
    } else if(this.cont==2){
      $('.progress .progress-right .progress-bar').css('animation-name', 'loading-4');
    } else if(this.cont==3){
      $('.progress .progress-right .progress-bar').css('animation-name', 'loading-8');
    } else if(this.cont==4){
      $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-4');
    } else if(this.cont==5){
      $('.progress.white .progress-left .progress-bar').css('animation-name', 'loading-8');
      this.router.navigateByUrl(`/paso3`);
    } else {

    }
    setTimeout(()=>{
        $('.progress-value').css('transform', 'scale(1)')
    }, 100);
    setTimeout(()=>{
        this.estado = 'pausa'
    }, 300);

  }

}

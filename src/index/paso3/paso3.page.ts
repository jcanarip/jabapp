import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paso3',
  templateUrl: './paso3.page.html',
  styleUrls: ['./paso3.page.scss'],
})
export class Paso3Page implements OnInit {
  swipeOpts = {
    allowSlidePrev: false,
    allowSlideNext: false
  };
  constructor(
      private router: Router
  ) {
      setTimeout(()=>{
          this.router.navigateByUrl(`/paso5`);
      }, 1000);
  }

  ngOnInit() {
  }

}
